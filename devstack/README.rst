========================================================
 Enabling qemu and libvirt from tar releases in Devstack
========================================================

1. Download DevStack

2. Add Virtual Open Systems' repo as an external repository::

     > cat local.conf
     [[local|localrc]]
     enable_plugin devstack-qemu-libvirt-from-tar-plugin https://git.virtualopensystems.com/sesame/devstack-qemu-libvirt-from-tar-plugin

3. run ``stack.sh``

Acknowledgements
---------------------
This work has been supported by the FP7 TRESCCA and H2020 SESAME projects,
under the grant numbers 318036 and 671596.
