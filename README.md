Installing libvirt and QEMU from sources
----------------------------------------

Virtual Open Systems devstack plugin
====================================

By default DevStack installs QEMU and libvirt from the OS' repository,
however sometimes there is a need to test OpenStack against the latest
version of these two programs.
The definition of the variables *LIBVIRT_VERSION* and *QEMU_VERSION*
in the **devstack/settings** file of this plugin will result in the compilation and
installation of libvirt and QEMU. E.g.,:

        QEMU_VERSION=2.1.6
        LIBVIRT_VERSION=1.3.3

The sources of the version pointed by these variables are downloaded
from the official libvirt and QEMU project websites, if not otherwise
specified by the variables *LIBVIRT_URL_BASE* and *QEMU_URL_BASE*
in the *settings* file. E.g.,:

	LIBVIRT_URL_BASE="http://www.virtualopensystems.com/sources/"
	QEMU_URL_BASE="http://www.virtualopensystems.com/sources/" 



*Note: OpenStack and its components (e.g. Nova) are mainly tested with the
repository version of both libvirt and QEMU.
Thus be aware that the use of a different version may lead to an unstable
or not working OpenStack environment.*


References
==========

Blueprint:
https://blueprints.launchpad.net/devstack/+spec/enable-qemu-libvirt-tars

Spec:
https://review.openstack.org/#/c/108714/
